package leetcode_test

import (
	"log"
	"testing"
)

func TestLetterCombos(t *testing.T) {
	for _, tc := range []struct {
		digits string
		expect []string
	}{
		{
			"23",
			[]string{"ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"},
		},
	} {
		t.Run(tc.digits, func(t *testing.T) {
			actual := letterCombinations(tc.digits)

			if !setsEqual(sliceToMap(actual), sliceToMap(tc.expect)) {
				t.Fatalf(
					"expected %+v for %s but got %+v",
					tc.expect, tc.digits, actual,
				)
			}

		})
	}
}

func sliceToMap(s []string) map[string]struct{} {
	m := map[string]struct{}{}
	for _, v := range s {
		m[v] = struct{}{}
	}
	return m
}

func setsEqual(m1, m2 map[string]struct{}) bool {
	if len(m1) != len(m2) {
		log.Printf("length m1(%d) != m2(%d)", len(m1), len(m2))
		return false
	}

	for k, v := range m1 {
		if v != m2[k] {
			return false
		}
	}

	return true
}

// solution follows:

var digitLetters = map[string]string{
	"2": "abc",
	"3": "def",
	"4": "ghi",
	"5": "jkl",
	"6": "mno",
	"7": "pqrs",
	"8": "tuv",
	"9": "wxyz",
}

func calcComboCount(digits string) int {
	c := 1
	for _, r := range digits {
		letters := digitLetters[string(r)]
		c *= len(letters)
	}
	return c
}

func letterCombinations(digits string) []string {
	if len(digits) < 1 {
		return nil
	}
	combos := make([]string, calcComboCount(digits))
	rLetterCombo(digits, combos)
	// log.Printf("combos: %+v", combos)
	return combos
}

// rLetterCombo will recursively calculating combos in chunks
func rLetterCombo(digits string, combos []string) {
	letters := digitLetters[string(digits[0])]
	// log.Printf("Digit: %s", string(digits[0]))
	// log.Printf("Combos: %+v size %d", combos, len(combos))
	for i, l := range letters {
		start := i * (len(combos) / len(letters))
		end := start + (len(combos) / len(letters))
		// log.Printf("chunk[%d:%d]", start, end)
		chunk := combos[start:end]
		for j, combo := range chunk {
			chunk[j] = combo + string(l)
		}
		if len(digits) > 1 {
			remain := digits[1:len(digits)]
			// log.Printf("remaining: %s", remain)
			rLetterCombo(remain, chunk)
		}
	}

	return
}
