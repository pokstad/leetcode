package leetcode_test

import "testing"

// TestIntToRoman is a solution for leet code problem:
// https://leetcode.com/problems/integer-to-roman/description/
func TestIntToRoman(t *testing.T) {
	for _, tc := range []struct {
		i  int
		rn string // roman numeral
	}{
		{1000, "M"},
		{3, "III"},
		{4, "IV"},
		{9, "IX"},
		{58, "LVIII"},
		{1994, "MCMXCIV"},
	} {
		t.Run(tc.rn, func(t *testing.T) {
			actual := intToRoman(tc.i)
			if actual != tc.rn {
				t.Fatalf("expected %s for %d but got %s", tc.rn, tc.i, actual)
			}
		})
	}
}

// numeralInt in ascending order
var numeralInt = []struct {
	s string
	i int
}{
	{"I", 1},
	{"IV", 4},
	{"V", 5},
	{"IX", 9},
	{"X", 10},
	{"XL", 40},
	{"L", 50},
	{"XC", 90},
	{"C", 100},
	{"CD", 400},
	{"D", 500},
	{"CM", 900},
	{"M", 1000},
}

func intToRoman(num int) string {
	var numStr string

	// iterate over all numerals in descending order
	for i := len(numeralInt); i > 0; i-- {
		n := numeralInt[i-1]
		for ; num >= n.i; num -= n.i {
			numStr += n.s
		}
	}

	return numStr
}
