package leetcode_test

import (
	"testing"

	"github.com/pokstad/leetcode/208_implement_trie"
)

type method int

const (
	insert method = iota
	search
	startsWith
)

type action struct {
	method method
	word   string
	result bool
}

func TestTrie(t *testing.T) {
	for _, actions := range [][]action{
		{
			{method: insert, word: "apple"},
			{method: search, word: "apple", result: true},
			{method: search, word: "app", result: false},
			{method: startsWith, word: "app", result: true},
			{method: insert, word: "app"},
			{method: search, word: "app", result: true},
		},
	} {
		trie := leetcode.Constructor()

		for _, a := range actions {
			t.Logf("Action: %#v", a)

			switch a.method {
			case insert:
				trie.Insert(a.word)
			case search:
				if trie.Search(a.word) != a.result {
					t.Errorf(
						"did not receive expected search result %t for %s",
						a.result, a.word,
					)
				}
			case startsWith:
				if trie.StartsWith(a.word) != a.result {
					t.Errorf(
						"did not receive expected prefix result %t for %s",
						a.result, a.word,
					)
				}
			default:
				t.Fatalf("invalid test case method name: %d", a.method)
			}
		}
	}
}
