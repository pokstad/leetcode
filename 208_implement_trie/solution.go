package leetcode

// Trie is a specialized tree structure for reTRIeval
type Trie struct {
	t        map[rune]*Trie
	complete bool
}

// Constructor is an objectionable function name since Go prefers "New".
// Leetcode needs to get their shit together.
func Constructor() Trie {
	return Trie{
		t: map[rune]*Trie{},
	}
}

// Insert will insert a word into the trie
func (t *Trie) Insert(word string) {
	current := t
	for _, l := range word {
		next, ok := current.t[l]
		if !ok {
			next = &Trie{
				t: map[rune]*Trie{},
			}
			current.t[l] = next
		}
		current = next
	}
	current.complete = true
}

// Search returns if the word is in the trie
func (t *Trie) Search(word string) bool {
	current := t
	for _, l := range word {
		next, ok := current.t[l]
		if !ok {
			return false
		}
		current = next
	}
	return current.complete
}

// StartsWith returns if there is any word in the trie that starts with the
// given prefix
func (t *Trie) StartsWith(prefix string) bool {
	current := t
	for _, letter := range prefix {
		next, ok := current.t[letter]
		if !ok {
			return false
		}
		current = next
	}
	return true
}

// Words will recursively find all possible words in the trie
func (t *Trie) Words(prefix string) []string {
	var words []string

	if t.complete {
		words = append(words, prefix)
	}

	for letter, trie := range t.t {
		// is this the end of a known word?
		prefix += string(letter)
		words = append(words, trie.Words(prefix)...)
	}
	return words
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
