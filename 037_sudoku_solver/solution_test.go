package main

import (
	"fmt"
	"testing"
)

func TestSudokuSolver(t *testing.T) {
	for _, tc := range []struct {
		name     string
		problem  [][]byte
		solution [][]byte
	}{
		{
			problem: [][]byte{
				[]byte("53..7...."),
				[]byte("6..195..."),
				[]byte(".98....6."),
				[]byte("8...6...3"),
				[]byte("4..8.3..1"),
				[]byte("7...2...6"),
				[]byte(".6....28."),
				[]byte("...419..5"),
				[]byte("....8..79"),
			},
			solution: [][]byte{
				[]byte("534678912"),
				[]byte("672195348"),
				[]byte("198342567"),
				[]byte("859761423"),
				[]byte("426853791"),
				[]byte("713924856"),
				[]byte("961537284"),
				[]byte("287419635"),
				[]byte("345286179"),
			},
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			solveSudoku(tc.problem)
			actualSolution := fmt.Sprintf("%#v", tc.problem)
			expectSolution := fmt.Sprintf("%#v", tc.solution)
			if actualSolution != expectSolution {
				t.Logf("Expect: %s", expectSolution)
				t.Logf("Actual: %s", actualSolution)
				t.Fatal("Expected solution does not match actual")
			}
		})
	}
}

func assertValidSolution(t *testing.T, board [][]byte) {

}

func TestBoxFor(t *testing.T) {
	for _, tc := range []struct {
		board [][]byte
		x     int
		y     int
		box   [][]byte
	}{
		{
			board: [][]byte{
				[]byte("53..7...."),
				[]byte("6..195..."),
				[]byte(".98....6."),
				[]byte("8...6...3"),
				[]byte("4..8.3..1"),
				[]byte("7...2...6"),
				[]byte(".6....28."),
				[]byte("...419..5"),
				[]byte("....8..79"),
			},
			x: 0,
			y: 0,
			box: [][]byte{
				[]byte("53."),
				[]byte("6.."),
				[]byte(".98"),
			},
		},
	} {
		actualBox := fmt.Sprintf("%+v", boxFor(tc.board, tc.x, tc.y))
		expectBox := fmt.Sprintf("%+v", tc.box)
		if actualBox != expectBox {
			t.Logf("actual: %s", actualBox)
			t.Logf("expect: %s", expectBox)
			t.Fatal("actual != expect box")
		}
	}
}
