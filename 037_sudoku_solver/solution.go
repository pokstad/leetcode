package main

// boxFor returns the sub-box for the tile at space (x,y)
func boxFor(board [][]byte, x, y int) [][]byte {
	x = (x / 3) * 3
	y = (y / 3) * 3

	return [][]byte{
		board[y+0][x : x+3],
		board[y+1][x : x+3],
		board[y+2][x : x+3],
	}
}

// allNums returns a set of all necessary numbers in sudoku
func allNums() map[byte]struct{} {
	return map[byte]struct{}{
		'1': struct{}{},
		'2': struct{}{},
		'3': struct{}{},
		'4': struct{}{},
		'5': struct{}{},
		'6': struct{}{},
		'7': struct{}{},
		'8': struct{}{},
		'9': struct{}{},
	}
}

// remainBox returns the remaining unused numbers for a given 3x3 box provided
func remainBox(box [][]byte, nums map[byte]struct{}) {
	for _, column := range box {
		for _, x := range column {
			delete(nums, x)
		}
	}
}

// remainCol returns all remaining values for column at 0-index x
func remainCol(board [][]byte, x int, nums map[byte]struct{}) {
	for _, row := range board {
		delete(nums, row[x])
	}
}

// remainRow returns all remaining values for row at 0-index y
func remainRow(board [][]byte, y int, nums map[byte]struct{}) {
	for _, col := range board[y] {
		delete(nums, col)
	}
}

func nextSpace(x, y int) (int, int) {
	x++
	x %= 9
	if x == 0 {
		y++
	}
	return x, y
}

func printBoard(board [][]byte) {
	// log.Printf("===========")
	// for _, row := range board {
	// 	log.Printf("|%s|", string(row))
	// }
}

// backtrack attempts to recursively solve a board by trying a combination
// and then backtracking when a combination does not yield a solution starting
// at the provided index.
//
// True is returned when the board is solved and the board will contain the
// solution, else false is returned with an unchanged board.
func backtrack(board [][]byte, x, y int) bool {
	printBoard(board)
	if y == 9 {
		// all spaces covered
		return true
	}

	xx, yy := nextSpace(x, y)

	if board[y][x] != '.' {
		//log.Printf("Position (%d, %d) is set to %c", x, y, board[y][x])
		return backtrack(board, xx, yy)
	}

	nums := allNums()
	remainBox(boxFor(board, x, y), nums)
	remainCol(board, x, nums)
	remainRow(board, y, nums)

	// save old value in case we need to backtrack
	old := board[y][x]

	// try all potential allowed values and their recursive possible solutions
	for n, _ := range nums {
		//log.Printf("attempting %c at position (%d, %d)", n, x, y)
		board[y][x] = n
		if backtrack(board, xx, yy) {
			// solution found!!!
			return true
		}
		//log.Printf("no solution exists for %c at (%d, %d)", board[y][x], x, y)
	}

	// backtrack
	//log.Printf("backtracking position (%d, %d) to %c", x, y, old)
	board[y][x] = old

	return false
}

// solveSudoku will attempt to solve a board using recursive backtracking. Order
// of box solving is from top left (0,0) to bottom right (2,2).
func solveSudoku(board [][]byte) {
	if !backtrack(board, 0, 0) {
		panic("no solution found")
	}
}
