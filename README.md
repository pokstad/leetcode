[![CircleCI](https://circleci.com/gh/pokstad/leetcode.svg?style=svg)](https://circleci.com/gh/pokstad/leetcode)

# LeetCode Solutions
My Golang solutions and test code to LeetCode problems: https://leetcode.com/pokstad/

I attempt to make my solutions reasonably performant with maximum readability with easy to read APIs.

All solutions adhere to the following standards:

- Simple table driven tests to verify results before submission.
- 100% standard library. No 3rd party dependencies.
- Green builds in CI.
